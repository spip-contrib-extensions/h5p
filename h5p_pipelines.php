<?php
function h5p_taches_generales_cron($taches){
	$taches['h5p_suppressionobsolettes'] = 24*3600; // tous les jours
	return $taches;
}
function h5p_formulaire_traiter($flux){
	if ($flux['args']['form'] == 'editer_document') {
		$id_document = $flux['data']['id_document'];
		// Est-on dans le cas d'un document déjà existant ?
		if ($id_document!='' AND is_numeric($id_document)) {
			$extension = sql_fetsel('extension', 'spip_documents',"id_document=$id_document");
			// Vérification que c'est un H5P
			if ($extension['extension']=='h5p') {
				$dir_extraction=_DIR_IMG.'h5p/extract/'.$id_document;
				// Si le répertoire d'extraction est présent, on le supprime pour laisser place à une éventuelle nouvelle version du fichier H5P qui sera décompréssée lors du prochain affichage
				if (@opendir($dir_extraction)) {
					RepEffaceh5p($dir_extraction);
				}
			}
		}
	}
	return $flux;
}

// On vide récursivement un répertoire, ses sous-répertoires et tous ses fichiers
function RepEffaceh5p($dir) {
    $handle = opendir($dir);
    while($elem = readdir($handle)) {
		if(is_dir($dir.'/'.$elem) && substr($elem, -2, 2) !== '..' && substr($elem, -1, 1) !== '.') {
            RepEffaceh5p($dir.'/'.$elem);
        }
        else {
            if(substr($elem, -2, 2) !== '..' && substr($elem, -1, 1) !== '.') {
                unlink($dir.'/'.$elem);
            }
        }   
    }
    $handle = opendir($dir);
    while($elem = readdir($handle)) {
        if(is_dir($dir.'/'.$elem) && substr($elem, -2, 2) !== '..' && substr($elem, -1, 1) !== '.') {
            RepEffaceh5p($dir.'/'.$elem);
            rmdir($dir.'/'.$elem);
        }
    }
    rmdir($dir);
}