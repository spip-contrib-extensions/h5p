# H5P



## Objectif

Ce plugin contient :

- des scripts permettant à SPIP de reconnaître les fichiers avec extension h5p
- un lecteur H5P permettant de lire à l'écran les animations H5P insérées (voir la documentation pour les manipulations d'incrustation : https://drane.ac-normandie.fr/diffuser-des-animations-h5p-sous-spip )

## Cas des plateformes mutualisées

Si vous installez ce plugin sur une plateforme mutualisée, il vous faudra probablement éditer au préalable le fichier inc/h5p_config.php afin de :
- commenter la ligne $retour['prefix_chemin_fichier']='';
- dé-commenter la ligne // $retour['prefix_chemin_fichier']='sites/'.$_SERVER['SERVER_NAME'];

## Vos demandes

Pour toute demande telle que l'ajout d'une bibliothèque manquante dans ce plugin, merci de m'adresser un mail : olivier.gautier@ac-normandie.fr
