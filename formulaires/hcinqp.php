<?php

// On fournit un fichier json et on repart avec un array de toutes les librairies dépendantes indiquées dans le json
function lister_les_librairie($json) {
	$liste=array();
	$h5p_array=json_decode(file_get_contents($json),true);
	if ($h5p_array['preloadedDependencies']!='') {
		$liste_des_dependances=$h5p_array['preloadedDependencies'];
		foreach ($liste_des_dependances as $dependance) {
			$liste[]=$dependance['machineName'].'-'.$dependance['majorVersion'].'.'.$dependance['minorVersion'];
		}
	}
	return $liste;
}

// On fournit un fichier json H5P et on repart avec son titre
function titre_h5p($json) {
	$titre='';
	$h5p_array=json_decode(file_get_contents($json),true);
	if ($h5p_array['title']!='') $titre=$h5p_array['title'];
	return $titre;
}

// On vide récursivement un répertoire, ses sous-répertoires et tous ses fichiers
function RepEfface($dir) {
    $handle = opendir($dir);
    while($elem = readdir($handle)) {
		if(is_dir($dir.'/'.$elem) && substr($elem, -2, 2) !== '..' && substr($elem, -1, 1) !== '.') {
            RepEfface($dir.'/'.$elem);
        }
        else {
            if(substr($elem, -2, 2) !== '..' && substr($elem, -1, 1) !== '.') {
                unlink($dir.'/'.$elem);
            }
        }   
    }
    $handle = opendir($dir);
    while($elem = readdir($handle)) {
        if(is_dir($dir.'/'.$elem) && substr($elem, -2, 2) !== '..' && substr($elem, -1, 1) !== '.') {
            RepEfface($dir.'/'.$elem);
            rmdir($dir.'/'.$elem);
        }
    }
    rmdir($dir);
}

// On vérifie que la liste du répertoire content est bien sur la WhiteList => 0, sinon nb>=1
function VerifContent($dir) {
	global $testVerifContent;
	$handle = opendir($dir);
	$defaultContentWhitelist = array('json','png','jpg','jpeg','gif','bmp','tif','tiff','svg','eot','ttf','woff','woff2','otf','webm','mp4','ogg','mp3','m4a','wav','txt','pdf','rtf','doc','docx','xls','xlsx','ppt','pptx','odt','ods','odp','xml','csv','diff','patch','swf','md','textile','vtt','webvtt','gltf','glb');
	while($elem = readdir($handle)) {
		if(is_dir($dir.'/'.$elem) && substr($elem, -2, 2) !== '..' && substr($elem, -1, 1) !== '.') {
			VerifContent($dir.'/'.$elem,$testContenu);
		}
		else {
			if(substr($elem, -2, 2) !== '..' && substr($elem, -1, 1) !== '.') {
				if (!in_array(strtolower(substr(strrchr($elem,'.'),1)),$defaultContentWhitelist)) {$testVerifContent++;}
			}
		}   
	}
	return $testVerifContent;
}

function presente_page_h5p($titre,$id,$dir_h5p) {
	echo '<!doctype html>
	<html lang="fr">
	<head>
	<meta charset="utf-8">
	<title>'.$titre.'</title>
	<style type="text/css">#spip-admin{display:none;}</style>
	</head>
	<body>
	<div id="h5p-'.$id.'"></div>
	<script type="text/javascript" src="'._DIR_PLUGIN_H5P.'main.bundle.js"></script>
	<script type="text/javascript">const el = document.getElementById("h5p-'.$id.'"); const options = {h5pJsonPath:  "./'.$dir_h5p.'",frameJs: "./'._DIR_PLUGIN_H5P.'frame.bundle.js",frameCss: "./'._DIR_PLUGIN_H5P.'styles/h5p.css",librariesPath: "./'._DIR_PLUGIN_H5P.'library", frame:false,copyright:true, embed:false, download:false, icon:false, export:false,';
	if (_request('fullscreen')=='non') {echo 'fullScreen:false,';} else {echo 'fullScreen:true,';}
	echo ' reportingIsEnabled:false, customCss:["'._DIR_PLUGIN_H5P.'styles/h5p-confirmation-dialog.css", "'._DIR_PLUGIN_H5P.'styles/h5p-core-button.css"]}; window.addEventListener("load",function(){new H5PStandalone.H5P(el, options);}); </script>
	</body>
	</html>
	';
}
function formulaires_hcinqp_charger_dist($id){
	include_spip('inc/h5p_config');
	$configuration=h5p_configuration();
	// Variable marqueur de la confirmité du fichier H5P (voir https://h5p.org/creating-your-own-h5p-plugin )
	$conformite_h5p=true;
	$message_erreur='';
	// Récupérer le nom du fichier dont l'id est $id
	$tab_sql=sql_fetsel('fichier','spip_documents',"id_document='".$id."'");
	$fichier=$tab_sql['fichier'];
	// Doc-dev : You should check that the file has an .h5p extension and then try to extract the file into a temporary directory using a library for handling Zip files.
	$chemin_fichier=_DIR_IMG.$fichier;
	$dir_extraction=_DIR_IMG.'h5p/extract/'.$id;
	$h5p_json=$dir_extraction.'/h5p.json';
	$h5p_json_long= $configuration['prefix_chemin_fichier'].'/'.$dir_extraction;
	// On vérifie tout d'abord si le répertoire d'extraction du H5P est bien présent. Cette vérification conditionne tout le reste des vérifications sur la présence des librairies et fichiers, voire la suppression du répertoire si le paquet n'est pas conforme.
	if (!@opendir($dir_extraction)) {
		mkdir($dir_extraction);

		// On s'assure de ne pas avoir encore extrait le H5P, opération lourde, si c'est déjà fait on ne refait pas
		if (!file_exists($h5p_json)) {
			$zip = new ZipArchive();
			if ($zip->open($chemin_fichier) === TRUE) {
				$zip->extractTo($dir_extraction);
				$zip->close();
			}
			else {
				$conformite_h5p=false;
				$message_erreur="L'installation du H5P ne peut se poursuivre car il n'a pas été possible d'en extraire le contenu. Le fichier a été supprimé du site.";
			}
			// On ne garde que le répertoire /content et le fichier h5p.json à la racine, tout le reste n'est que librairie et doit être supprimé : pas de librairie perso admise par sécurité
			$rep_sup = opendir($dir_extraction);
			while($elem = readdir($rep_sup)) {
				if ($elem!='.' AND $elem!='..' AND $elem!='content' AND $elem!='h5p.json') {
					// Si des fichiers pourris sont placés à la racine, on les supprime
					if (!is_dir($dir_extraction.'/'.$elem)) {unlink($dir_extraction.'/'.$elem);}
					// Pour le reste, ce sont des répertoires donc on les supprime récursivement
					else {RepEfface($dir_extraction.'/'.$elem);}
				}
			}
		}
		if (file_exists($h5p_json)) {
			// On teste la présence des librairies utiles
			$liste_librairies=lister_les_librairie($h5p_json);
			$librairies_pourries=array();
			foreach ($liste_librairies as $cle=>$lib) {
				if(!is_dir(_DIR_PLUGIN_H5P.'library/'.$lib)){
					$librairies_pourries[]=$lib;
					$conformite_h5p=false;
				}
			}
			if ($librairies_pourries[1]!='') {
				$liste_lib='les biblioth&#232;ques <b>'.implode(', ', $librairies_pourries).'</b> sont absentes';
				$singplu_lib='les noms de ces biblioth&#232;ques pour qu\'il examine leur ajout au plugin.';
				$mail="mailto:olivier.gautier@ac-normandie.fr?subject=Ajout de biblioth&#232;ques H5P&body=Pouvez-vous ajouter les biblioth&#232;ques ".implode(', ', $librairies_pourries)." au plugin H5P &#63;";
			}
			else {
				$liste_lib='la biblioth&#232;que <b>'.$librairies_pourries[0].'</b> est absente';
				$singplu_lib='le nom de cette biblioth&#232;que pour qu\'il examine son ajout au plugin.';
				$mail="mailto:olivier.gautier@ac-normandie.fr?subject=Ajout d&#8217;une biblioth&#232;que H5P&body=Pouvez-vous ajouter la biblioth&#232;que ".$librairies_pourries[0]." au plugin H5P &#63;";
			}
			$message_erreur="<p>L'installation du H5P ne peut se poursuivre car $liste_lib du plugin H5P.<br>Merci de le signaler à <a href='$mail'>Olivier Gautier</a>, créateur de ce plugin, en précisant $singplu_lib</p><p>Le fichier a été supprimé du site.</p>";
		}
		// Si pas de h5p.json dans le projet, alors non conforme
		else {
			$conformite_h5p=false;
			$message_erreur="L'installation du H5P ne peut se poursuivre car votre fichier H5P ne contient pas les éléments nécessaires à son exécution. Le fichier a été supprimé du site.";
		}
		
		if ($conformite_h5p) {
			// On continue en vérifiant la présence de content/content.json, si pas présent bye bye
			if (!file_exists($h5p_content_json=$dir_extraction.'/content/content.json')) {$conformite_h5p=false;}
			// On vérifie les extensions de tous les fichiers de /content en vérifiant qu'ils sont bien dans la WhiteList, sinon bye bye
			$testVerifContent=0;
			if (VerifContent($dir_extraction)!= 0) {
				$message_erreur = 'Votre activité H5P contient un ou plusieurs fichiers compressés dont les extensions ne sont pas autorisées, il a été supprimé du site.';
				$conformite_h5p=false;
			}
		}
		
		// On poursuit en vérifiant le contenu de h5p.json, histoire qu'il ne contienne pas de cochonneries
		if ($conformite_h5p) {
			$contenu_json = json_decode(file_get_contents($h5p_json),true);
			
			// On commence par les paramètres obligatoires
			$verifH5pRequired = array (
				array($contenu_json['title'],'/^.{1,255}$/'),
				array($contenu_json['language'],'/^[-a-zA-Z]{1,10}$/'),
				array($contenu_json['preloadedDependencies'][0]['machineName'],'/^[\w0-9\-\.]{1,255}$/i'),
				array($contenu_json['preloadedDependencies'][0]['majorVersion'],'/^[0-9]{1,5}$/'),
				array($contenu_json['preloadedDependencies'][0]['minorVersion'],'/^[0-9]{1,5}$/'),
				array($contenu_json['mainLibrary'],'/^[$a-z_][0-9a-z_\.$]{1,254}$/i'),
			);	
			// Et ceux optionnels
			if ($contenu_json['contentType'] != '') {$verifH5pRequired[] = array($contenu_json['contentType'],'/^.{1,255}$/');}
			if ($contenu_json['dynamicDependencies'][0]['machineName'] != '') {$verifH5pRequired[] = array($contenu_json['dynamicDependencies'][0]['machineName'],'/^[\w0-9\-\.]{1,255}$/i');}
			if ($contenu_json['dynamicDependencies'][0]['majorVersion'] != '') {$verifH5pRequired[] = array($contenu_json['dynamicDependencies'][0]['majorVersion'],'/^[0-9]{1,5}$/');}
			if ($contenu_json['dynamicDependencies'][0]['minorVersion'] != '') {$verifH5pRequired[] = array($contenu_json['dynamicDependencies'][0]['minorVersion'],'/^[0-9]{1,5}$/');}
			
			foreach ($verifH5pRequired as $tab) {
				if (filter_var($tab[0], FILTER_VALIDATE_REGEXP,array("options" => array("regexp"=>$tab[1])))!=$tab[0]) {
					$message_erreur = 'Certains éléments de votre activité H5P contiennent des caractères non autorisés. Le fichier a été supprimé du site.';
					$conformite_h5p=false;
				}
			}
			if (!in_array($contenu_json['embedTypes'][0],array('iframe','div'))) {
				$message_erreur = 'Certains éléments de votre activité H5P contiennent des caractères non autorisés. Le fichier a été supprimé du site.';
				$conformite_h5p=false;
			}
		}

		// Pour les packages non conformes, on supprime l'archive extraite, le fichier H5P, les entrées en BDD et on informe le rédacteur
		if (!$conformite_h5p) {
			RepEfface($dir_extraction);
			unlink($chemin_fichier);
			echo "<head><style type='text/css'>#spip-admin{display:none;}</style></head>
			<div style='display:block; border-left:10px #F5C000 solid; margin:0; padding:20px; background-color:#FCF0C5; color:#000; border-radius:12px;'></p>";
			if ($message_erreur) {echo $message_erreur;}
			else {echo "Le fichier h5p n'est pas conforme aux attendus du plugin H5P, il a été supprimé du site.";}
			echo "</p></div>";
			// On supprime les entrées en BDD
			sql_delete('spip_documents_liens', 'id_document = '.$id);
			sql_delete('spip_documents', 'id_document = '.$id);
			// Ce serait bien de faire un reload ajax de la zone des documents pour matérialiser la suppression mais est-ce possible, le bloc ajax ne semble pas être clairement identifié dans les squelettes ?
		}
	}
	// Toutes les vérifications sont faites, on peut poursuivre
	if ($conformite_h5p) {
		//Titre du H5P
		$titre_h5p=titre_h5p($h5p_json);
		// On présente maintenant le code permettant l'affichage du H5P en iframe
		presente_page_h5p($titre_h5p,$id,$h5p_json_long);
	}
}
function formulaires_hcinqp_verifier_dist($id){

}
function formulaires_hcinqp_traiter_dist($id){}