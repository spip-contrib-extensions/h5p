<?php
// Effacement des répertoires récursivement
function RepEfface($dir) {
    $handle = opendir($dir);
    while($elem = readdir($handle)) {
		if(is_dir($dir.'/'.$elem) && substr($elem, -2, 2) !== '..' && substr($elem, -1, 1) !== '.') {
            RepEfface($dir.'/'.$elem);
        }
        else {
            if(substr($elem, -2, 2) !== '..' && substr($elem, -1, 1) !== '.') {
                unlink($dir.'/'.$elem);
            }
        }   
    }
    $handle = opendir($dir);
    while($elem = readdir($handle)) {
        if(is_dir($dir.'/'.$elem) && substr($elem, -2, 2) !== '..' && substr($elem, -1, 1) !== '.') {
            RepEfface($dir.'/'.$elem);
            rmdir($dir.'/'.$elem);
        }
    }
    rmdir($dir);
}

// Cron qui nettoie les décompressions de H5P qui ne sont plus présents dans la base spip_documents
function genie_h5p_suppressionobsolettes_dist($t) {
	$dir_extraction=_DIR_IMG.'h5p/extract/';
	$handle = opendir($dir_extraction);
	$texte='';
    while($id = readdir($handle)) {
		$rep=$dir_extraction.'/'.$id;
		if(is_dir($rep) && substr($id, -2, 2) !== '..' && substr($id, -1, 1) !== '.') {
			$test_document=sql_select('extension','spip_documents',"id_document=$id AND extension='h5p'");
			$tab_document=sql_fetch($test_document);
			$test_presence_h5p=$tab_document['extension'];
			if ($test_presence_h5p!='h5p') RepEfface($rep);
		}
	}
	return 1;
}